export ZSH="/home/marinos/.oh-my-zsh"
ZSH_THEME="bureau"
plugins=(git)
source $ZSH/oh-my-zsh.sh

export EDITOR="vim"
export SUDO_ASKPASS="/usr/local/bin/:dpass"

export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
export _JAVA_AWT_WM_NONREPARENTING=1

_SILENT_JAVA_OPTIONS="$_JAVA_OPTIONS"
unset _JAVA_OPTIONS
alias java='java "$_SILENT_JAVA_OPTIONS"'

alias :pb="curl -F 'f:1=<-' ix.io"
alias ls='ls --color=auto'
alias ll='ls -l'
alias :q='exit'
alias :search='sudo pacman -Ss'
alias :install='sudo pacman -S'
alias :update='sudo pacman -Sy'
alias :upgrade='sudo pacman -Syu'
alias :remove='sudo pacman -R'
alias :purge='sudo pacman -Rsn'
