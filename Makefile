include config.mk

SRC = target.c
OBJ = ${SRC:.c=.o}

all: options target

options:
	@echo target build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.mk

getxkblayout: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f target ${OBJ} 

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f target ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/target

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/target

.PHONY: all options clean install uninstall
