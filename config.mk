# target version
VERSION = 1.0

# paths
PREFIX = /usr/local

# includes and libs
INCS = -I/usr/include 
LIBS = -l

CFLAGS   = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Os ${INCS}
LDFLAGS  = ${LIBS}

# compiler and linker
CC = cc
