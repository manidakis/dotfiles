call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'flazz/vim-colorschemes'
Plug 'itchyny/lightline.vim'
Plug 'amiorin/vim-project'
Plug 'StanAngeloff/php.vim'
Plug 'stephpy/vim-php-cs-fixer'
Plug 'phpactor/phpactor'
Plug 'shawncplus/phpcomplete.vim'

Plug 'eshion/vim-sync'
Plug 'skywind3000/asyncrun.vim'

call plug#end()

filetype plugin indent on
set omnifunc=syntaxcomplete#Complete

colorscheme gruvbox
set mouse=a
set shortmess+=c
set number
set ruler
set cmdheight=2
set backspace=eol,start,indent
set ignorecase
set hlsearch
set lazyredraw
set magic
set showmatch
set noerrorbells
set novisualbell
set t_vb=
set tm=500
syntax enable
set t_Co=256
set background=dark
set encoding=utf8
set ffs=unix,dos,mac
set nobackup
set nowb
set noswapfile
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set lbr
set tw=500
set ai              " Auto indent
set si              " Smart indent
set nowrap          " No wrap lines
set laststatus=2    " Fix last status always on for lightline.vim
inoremap <c-c> <ESC>
nnoremap <Leader>cl :let @/ = ""<CR>
set clipboard=unnamed

" Cycle through all buffers
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprevious<CR>

" Fugitive
set diffopt+=vertical
nnoremap <Leader>gs :Gstatus<CR>
nnoremap <Leader>gd :Gdiff<CR>
nnoremap <leader>gc :Gcommit<CR>
nnoremap <leader>gl :Glog<CR>
set splitbelow

" NERDTree
nnoremap <F1> :NERDTreeToggle<CR>
nnoremap ,n :NERDTreeFind<CR>
let g:NERDTreeDirArrowExpandable = '+'
let g:NERDTreeDirArrowCollapsible = '-'

" Function keys for general purpose
nnoremap <F2> :w<CR>
inoremap <F2> <ESC>:w<CR>i

" Easy window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
let g:C_Ctrl_j = 'off'

" Quickly search for matching words in document
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

" Automatic ctags generation on file save if on git repo
au BufWritePost *.php silent! !eval '[ -f ".git/hooks/ctags" ] && .git/hooks/ctags' &

" PHP CS Fixer
let g:php_cs_fixer_rules = '@PSR2'
let g:php_cs_fixer_path = '/home/marinos/.config/composer/vendor/bin/php-cs-fixer'
let g:php_cs_fixer_php_path = "php"
let g:php_cs_fixer_dry_run = 0
let g:php_cs_fixer_verbose = 1
nnoremap <silent><leader>pcd :call PhpCsFixerFixDirectory()<CR>
nnoremap <silent><leader>pcf :call PhpCsFixerFixFile()<CR>
autocmd BufWritePost *.php silent! call PhpCsFixerFixFile() " Automatic fix the file on *.php filesaves

" PHP ACTOR key bindings
" Include use statement
nmap <Leader>u :call phpactor#UseAdd()<CR>

" Invoke the context menu
nmap <Leader>mm :call phpactor#ContextMenu()<CR>

" Invoke the navigation menu
nmap <Leader>nn :call phpactor#Navigate()<CR>

" Goto definition of class or class member under the cursor
nmap <Leader>o :call phpactor#GotoDefinition()<CR>

" Show brief information about the symbol under the cursor
nmap <Leader>K :call phpactor#Hover()<CR>

" Transform the classes in the current file
nmap <Leader>tt :call phpactor#Transform()<CR>

" Generate a new class (replacing the current file)
nmap <Leader>cc :call phpactor#ClassNew()<CR>

" Extract expression (normal mode)
nmap <silent><Leader>ee :call phpactor#ExtractExpression(v:false)<CR>

" Extract expression from selection
vmap <silent><Leader>ee :<C-U>call phpactor#ExtractExpression(v:true)<CR>

" Extract method from selection
vmap <silent><Leader>em :<C-U>call phpactor#ExtractMethod()<CR>



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! CmdLine(str)
    call feedkeys(":" . a:str)
endfunction 

function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", "\\/.*'$^~[]")
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ack '" . l:pattern . "' " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

function! SetupSync(...)
    let g:sync_exe_filenames    = 'remote.sync;'    " remote.sync file is an executable containing the connection properties
    let g:sync_async_upload     = 1                 " Async upload

    augroup SyncGroup
        autocmd!
        autocmd BufWritePost * :call SyncUploadFile()
        map <F7> gg=G<C-o><C-o>
    augroup END

endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Projects
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:project_use_nerdtree = 1
set rtp+=~/.vim/bundle/vim-project/
call project#rc("~/projects")

Project     '~/iwv/projects/easydebt'
Callback    'easydebt'                          , 'SetupSync'

Project     '~/iwv/projects/online-timologia'
Callback    'online-timologia'                  , 'SetupSync'

Project     '~/iwv/projects/MEA'
Callback    'MEA'                               , 'SetupSync'

Project     '~/iwv/projects/EbayAPI'
Callback    'EbayAPI'                           , 'SetupSync'
